package com.example.demo.controller;

import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/")
public class TestController {

    @GetMapping
    String testResponse () {
        return "Just testing again! (Test)???????????????????????????????????";
    }

    @GetMapping("/products/{id}")
    String getProducts (@PathVariable String id) {
        return id;
    }
}
